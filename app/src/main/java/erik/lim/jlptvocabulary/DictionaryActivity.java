package erik.lim.jlptvocabulary;

import android.content.Context;
import android.content.SharedPreferences;
import android.databinding.DataBindingUtil;
import android.speech.tts.TextToSpeech;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.CompoundButton;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import erik.lim.jlptvocabulary.database.DatabaseAccess;
import erik.lim.jlptvocabulary.databinding.ActivityMainBinding;
import erik.lim.jlptvocabulary.model.Vocabulary;

public class DictionaryActivity extends AppCompatActivity {

    private static final String PREF_NAME = "settingsprefs";
    private static final String KEY_SPEECH_ENABLED = "key_speech_enabled";
    private static final String ACTIVITY_STATE_INDEX = "activity_state_index";
    private static final String APPLICATION_LAST_KNOWN_INDEX = "application_last_known_index";

    private DatabaseAccess databaseAccess;
    private ActivityMainBinding activityMainBinding;
    private TextToSpeech textToSpeech;
    private List<Vocabulary> vocabularyList = new ArrayList<>();
    private int index = 0;
    private SharedPreferences sharedPreferences;
    private boolean shouldSpeak = true;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityMainBinding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        sharedPreferences = getSharedPreferences(PREF_NAME, 0);

        textToSpeech = new TextToSpeech(getApplicationContext(), new TextToSpeech.OnInitListener() {
            @Override
            public void onInit(int i) {
                if (i != TextToSpeech.ERROR) {
                    textToSpeech.setLanguage(Locale.JAPANESE);
                }
            }
        });
        //init db
        databaseAccess = DatabaseAccess.getInstance(this);
        databaseAccess.open();
        vocabularyList = databaseAccess.getVocabularyList();

        index = sharedPreferences.getInt(APPLICATION_LAST_KNOWN_INDEX, 0);
        showVocabularyItem();

        activityMainBinding.nextButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                iterateIndex();
                showVocabularyItem();
            }
        });

        activityMainBinding.backButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                decrementIndex();
                showVocabularyItem();
            }
        });

        activityMainBinding.playButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Vocabulary currentItem = getCurrentItem();
                pronounceWord(currentItem);
            }
        });

        activityMainBinding.checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                Vocabulary currentItem = getCurrentItem();
                currentItem.bookmark = b;
                databaseAccess.updateVocabularyItem(currentItem);
            }
        });

        shouldSpeak = sharedPreferences.getBoolean(KEY_SPEECH_ENABLED, false);
        activityMainBinding.speechSettingSwitch.setChecked(shouldSpeak);
        activityMainBinding.speechSettingSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                sharedPreferences.edit().putBoolean(KEY_SPEECH_ENABLED, b).commit();
                shouldSpeak = b;
            }
        });
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt(ACTIVITY_STATE_INDEX, index);
    }

    @Override
    protected void onRestoreInstanceState(Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);
        if (savedInstanceState != null) {
            index = savedInstanceState.getInt(ACTIVITY_STATE_INDEX);
            showVocabularyItem();
        }
    }

    private void iterateIndex() {
        if (index < vocabularyList.size()-1) {
            index++;
            storeLastKnownIndex(index);
        }
    }

    private void storeLastKnownIndex(int index) {
        sharedPreferences.edit().putInt(APPLICATION_LAST_KNOWN_INDEX, index).commit();
    }

    private void decrementIndex() {
        if (index > 0) {
            index--;
            storeLastKnownIndex(index);
        }
    }

    private Vocabulary getCurrentItem() {
        return vocabularyList.get(index);
    }

    private void showVocabularyItem() {
        Vocabulary currentItem = getCurrentItem();
        activityMainBinding.kanjiView.setText(currentItem.kanji);
        activityMainBinding.kanaView.setText(currentItem.kana);
        activityMainBinding.meaning.setText(currentItem.meaning);
        activityMainBinding.checkBox.setChecked(currentItem.bookmark);
        pronounceWord(currentItem);

    }

    private void pronounceWord(Vocabulary currentItem) {
        if (!textToSpeech.isSpeaking() && shouldSpeak) {
            textToSpeech.speak(currentItem.kana, TextToSpeech.QUEUE_FLUSH, null);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        databaseAccess.close();
    }
}
