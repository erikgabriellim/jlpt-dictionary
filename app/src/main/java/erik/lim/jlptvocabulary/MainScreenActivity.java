package erik.lim.jlptvocabulary;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityManagerCompat;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;

import erik.lim.jlptvocabulary.databinding.ActivityMainScreenBinding;

public class MainScreenActivity extends AppCompatActivity {

    ActivityMainScreenBinding activityMainScreenBinding;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        activityMainScreenBinding = DataBindingUtil.setContentView(this, R.layout.activity_main_screen);

        activityMainScreenBinding.dictionaryButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainScreenActivity.this, DictionaryActivity.class);
                startActivity(intent);
            }
        });

        activityMainScreenBinding.bookmarkButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent intent = new Intent(MainScreenActivity.this, BookmarkedVocabularyActivity.class);
                startActivity(intent);
            }
        });
    }

}
