package erik.lim.jlptvocabulary.database;

/**
 * Created by erikgabriellim on 7/20/16.
 */
import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;
import java.util.List;

import erik.lim.jlptvocabulary.model.Vocabulary;

public class DatabaseAccess {
    private SQLiteOpenHelper openHelper;
    private SQLiteDatabase database;
    private static String vocabularyTableName = "vocabulary";
    private static DatabaseAccess instance;

    /**
     * Private constructor to aboid object creation from outside classes.
     *
     * @param context
     */
    private DatabaseAccess(Context context) {
        this.openHelper = new DatabaseOpenHelper(context);
    }

    /**
     * Return a singleton instance of DatabaseAccess.
     *
     * @param context the Context
     * @return the instance of DabaseAccess
     */
    public static DatabaseAccess getInstance(Context context) {
        if (instance == null) {
            instance = new DatabaseAccess(context);
        }
        return instance;
    }

    /**
     * Open the database connection.
     */
    public void open() {
        this.database = openHelper.getWritableDatabase();
    }

    /**
     * Close the database connection.
     */
    public void close() {
        if (database != null) {
            this.database.close();
        }
    }

    public void updateVocabularyItem(Vocabulary item) {
        ContentValues cv = new ContentValues();
        cv.put("bookmark", item.bookmark ? 1 : 0);
        database.update(vocabularyTableName, cv, "id=?", new String[]{Integer.toString(item.id)});
    }

    public List<Vocabulary> getBookmarkedItemList() {
        List<Vocabulary> list = new ArrayList<>();
        Cursor cursor = database.rawQuery("SELECT * FROM vocabulary where bookmark <> 0", null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Vocabulary item = new Vocabulary();
            item.id = cursor.getInt(0);
            item.kanji = cursor.getString(1);
            item.kana = cursor.getString(2);
            item.romaji = cursor.getString(3);
            item.meaning = cursor.getString(4);
            item.partOfSpeech = cursor.getString(5);
            item.bookmark = cursor.getInt(6) == 1;
            list.add(item);
            cursor.moveToNext();
        }
        cursor.close();
        return list;
    }

    public List<Vocabulary> getVocabularyList() {
        List<Vocabulary> list = new ArrayList<>();
        Cursor cursor = database.rawQuery("SELECT * FROM vocabulary", null);
        cursor.moveToFirst();
        while (!cursor.isAfterLast()) {
            Vocabulary item = new Vocabulary();
            item.id = cursor.getInt(0);
            item.kanji = cursor.getString(1);
            item.kana = cursor.getString(2);
            item.romaji = cursor.getString(3);
            item.meaning = cursor.getString(4);
            item.partOfSpeech = cursor.getString(5);
            item.bookmark = cursor.getInt(6) == 1;
            list.add(item);
            cursor.moveToNext();
        }
        cursor.close();
        return list;
    }
}