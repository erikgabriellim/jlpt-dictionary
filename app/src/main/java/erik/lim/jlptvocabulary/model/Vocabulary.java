package erik.lim.jlptvocabulary.model;

/**
 * Created by erikgabriellim on 7/20/16.
 */
public class Vocabulary {
    public int id;
    public String kanji;
    public String kana;
    public String romaji;
    public String meaning;
    public String partOfSpeech;
    public boolean bookmark = false;
}
